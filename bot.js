const settings = require('./settings.json');
const DiscordAPI = require('discord.js');
const fs = require('fs');
const colors = require('colors');

let client = new DiscordAPI.Client({disableEveryone: true});
client.login(settings.token);

let prefix = settings.prefix;

client.on("ready", async () => {
    console.log(`Bot is online! ${client.user.username} --\nclientid:${settings.clientid}\ntoken:${settings.token}\n`.red);
    client.user.setActivity(`Create process..`, {type: "WATCHING"});
});

client.on("message", async message => {
    if(message.author.bot) return;
    let content = message.content.split(" ");
    let args = content.slice(1);
    let command = content[0];
    if (message.content.startsWith(prefix) === false) return;
    if (content[0] == prefix+"test") {
        message.channel.send("It's okey")
    }
    let commandfile = client.commands.get(command.slice(prefix.length));
    if(commandfile) commandfile.run(client,message,args);
    if(message.channel.type === 'dm') {
      console.log(message.content)
    }
});

client.commands = new DiscordAPI.Collection();

fs.readdir("./commands/", (err, files) => {

    if(err) console.log(err);

    let jsfile = files.filter(f => f.split(".").pop() === "js");
    if(jsfile.length <= 0){
        console.log("Couldn't find commands.");
        return;
    }

    jsfile.forEach((f) =>{
        let props = require(`./commands/${f}`);
        console.log(`File-[${f}]-Loaded!`.yellow);
        client.commands.set(props.help.name, props);
    });

});
